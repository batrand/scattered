﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Memory : MonoBehaviour
{
    public int AddSeconds = 5;
    public int ExpireSeconds = 5;
    private DementiaMeter _dementiaMeter;
    private void Start()
    {
        _dementiaMeter = GameObject.FindGameObjectWithTag("DementiaMeter").GetComponent<DementiaMeter>();
        InvokeRepeating(nameof(DestroySelf), ExpireSeconds, 0);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _dementiaMeter.AddSeconds(AddSeconds);
            Destroy(gameObject);
        }
    }

    private void DestroySelf() => Destroy(gameObject);
}
