﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    /// <summary>
    /// The Door that this Door leads to
    /// </summary>
    public Door Target;

    /// <summary>
    /// The position for other Doors to exit to.
    /// </summary>
    public Transform ExitPoint;

    /// <summary>
    /// Is this door dissolved and this blocked?
    /// </summary>
    public bool IsDissolved = false;

    /// <summary>
    /// The player is currently selecting this door
    /// </summary>
    private bool _isActive;

    private Transform _player;

    public GameObject ButtonHint;

    private void Update()
    {
        if (Input.GetKeyDown("space") && _isActive)
        {
            if (Target.IsDissolved) return;
            _player.position = Target.ExitPoint.position;
            _isActive = false;
        }

        ButtonHint.SetActive(_isActive);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _isActive = true;
            _player = other.gameObject.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _isActive = false;
    }
}
