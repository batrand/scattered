﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string MenuPath = "Scenes/MenuScene";
    public string CreditPath = "Scenes/CreditScene";
    public string GamePath = "Scenes/GameScene";

    public void LoadMenu() => SceneManager.LoadScene(MenuPath);
    public void LoadGame() => SceneManager.LoadScene(GamePath);
    public void LoadCredit() => SceneManager.LoadScene(CreditPath);
}
