﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour
{
    public GameObject BlinkTarget;
    public float Interval = 1;

    public void Start() => StartCoroutine(BlinkCoroutine());

    public bool ShouldBlink = false;
    private IEnumerator BlinkCoroutine()
    {
        while (ShouldBlink)
        {
            BlinkTarget.SetActive(!BlinkTarget.activeSelf);
            yield return new WaitForSeconds(Interval);
        }
        BlinkTarget.SetActive(false);
    }
}
