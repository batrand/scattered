﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Clock : MonoBehaviour
{
    private float _secondsSinceLoad = 0;

    public string DisplayString => $"{TimeSpan.FromSeconds(_secondsSinceLoad):hh\\:mm\\:ss}";

    private Text _label;
    private void Start()
    {
        _label = GetComponent<Text>();
    }

    private void Update()
    {
        _secondsSinceLoad = Time.timeSinceLevelLoad;
        _label.text = DisplayString;
    }
}
