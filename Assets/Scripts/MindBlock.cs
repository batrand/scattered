﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MindBlock : MonoBehaviour
{
    public MindBlock NextBlock;
    public bool DissolveAtStart = false;
    public float DissolveRate = 0.1f;
    public GameObject SelectionMarker;
    public GameObject TotallyDissolvedPrefab;
    public DissolutionEffect Dissolver;
    [Range(0,1)]
    public float DissolutionAmount = 0;

    public bool IsTotallyDissolved = false;
    public bool HasStartedDissolving = false;
    public event EventHandler JustTotallyDissolved;

    private void Start()
    {
        if(DissolveAtStart) StartDissolution();
    }

    private void Update()
    {
        Dissolver.SetAmount(DissolutionAmount);
    }

    public void AddDissolution(float amount)
    {
        DissolutionAmount += amount;
        DissolutionAmount = Mathf.Clamp(DissolutionAmount, 0, 1);
        Dissolver.SetAmount(DissolutionAmount);
        if (DissolutionAmount >= 1)
        {
            IsTotallyDissolved = true;
            JustTotallyDissolved?.Invoke(this, null);
            Instantiate(TotallyDissolvedPrefab, transform.position, transform.rotation);
            if(NextBlock != null) NextBlock.StartDissolution();
        }
    }

    public void RemoveDissolution(float amount)
    {
        if (IsTotallyDissolved) return;
        DissolutionAmount -= amount;
        DissolutionAmount = Mathf.Clamp(DissolutionAmount, 0, 1);
        Dissolver.SetAmount(DissolutionAmount);
    }

    public void StartDissolution() => StartCoroutine(DissolveCoroutine());

    private IEnumerator DissolveCoroutine()
    {
        HasStartedDissolving = true;
        while (!IsTotallyDissolved)
        {
            AddDissolution(DissolveRate);
            yield return new WaitForSeconds(1);
        }
    }

    public void Select()
    {
        if (IsTotallyDissolved) return;
        SelectionMarker.SetActive(true);
    }

    public void Deselect() => SelectionMarker.SetActive(false);
}
