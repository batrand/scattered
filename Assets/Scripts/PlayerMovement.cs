﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D Controller;
    public float Speed = 40f;

    private float _horizontalMove = 0f;
    private bool _jump = true;

    public bool IsMoving = false;

    private void Update()
    {
        _horizontalMove = Input.GetAxisRaw("Horizontal") * Speed;
        if (Input.GetButtonDown("Jump")) _jump = true;

        IsMoving = !_horizontalMove.Equals(0);
    }

    private void FixedUpdate()
    {
        Controller.Move(_horizontalMove, false, _jump);
        _jump = false;
    }
}
