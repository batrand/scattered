﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public DementiaMeter Dementia;
    public MindState Mind;

    public string GameScenePath = "Scenes/GameScene";
    public string MenuScenePath = "Scenes/MenuScene";

    public Clock Clock;
    public GameObject GameOverPanel;
    public Text ClockLabel;

    private void Start()
    {
        Dementia.TimesUp += OnGameOver;
        Mind.AllBlocksDissolved += OnGameOver;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) Exit();
    }

    private void OnGameOver(object sender, System.EventArgs e)
    {
        ClockLabel.text = $"You stayed sane for {Clock.DisplayString}";
        GameOverPanel.SetActive(true);
    }

    public void Restart() => SceneManager.LoadScene(GameScenePath);
    public void Exit() => SceneManager.LoadScene(MenuScenePath);
}
