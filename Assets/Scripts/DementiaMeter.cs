﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DementiaMeter : MonoBehaviour
{
    public int StartingSeconds = 60;
    public int RemainingSeconds { get; private set; }
    public event EventHandler TimesUp;

    public int PortionSize = 20;
    public event EventHandler FitHappened;

    private void Start()
    {
        RemainingSeconds = StartingSeconds;
        InvokeRepeating(nameof(TickDown), 1, 1);
    }

    private void TickDown()
    {
        RemainingSeconds--;
        if (RemainingSeconds < 0) RemainingSeconds = 0;
        //Debug.Log($"Remaining seconds: {RemainingSeconds}");

        if (RemainingSeconds % PortionSize == 0)
        {
            FitHappened?.Invoke(this, null);
            Debug.Log("One portion's up!");
        }

        if (RemainingSeconds <= 0)
        {
            TimesUp?.Invoke(this, null);
            CancelInvoke(nameof(TickDown));
        }
    }

    public void AddSeconds(int seconds)
    {
        RemainingSeconds += seconds;
        if (RemainingSeconds > StartingSeconds) RemainingSeconds = StartingSeconds;
        Debug.Log($"Remaining seconds: {RemainingSeconds}");
    }
}
