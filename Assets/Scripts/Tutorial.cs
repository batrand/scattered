﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject TutorialPanel;
    private void Start()
    {
        Time.timeScale = 0;
    }

    public void Dismiss()
    {
        Time.timeScale = 1;
        Destroy(TutorialPanel);
    }
}
