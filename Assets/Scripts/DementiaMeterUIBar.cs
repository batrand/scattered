﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DementiaMeterUIBar : MonoBehaviour
{
    public DementiaMeter Dementia;
    public RectTransform Bar;

    public Image BarImage;
    public Color Healthy;
    public Color Danger;

    private float _maxWidth;
    private float _barY;
    private void Start()
    {
        _maxWidth = Bar.sizeDelta.x;
        _barY = Bar.sizeDelta.y;
    }

    private void Update()
    {
        var barPercentage = (float) Dementia.RemainingSeconds / (float) Dementia.StartingSeconds;
        Bar.sizeDelta = new Vector2(barPercentage * _maxWidth, _barY);

        if (Dementia.RemainingSeconds > (Dementia.StartingSeconds / 2))
            BarImage.color = Healthy;
        else BarImage.color = Danger;
    }
}
