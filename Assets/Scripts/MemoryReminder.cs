﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryReminder : MonoBehaviour
{
    public DementiaMeter Dementia;
    public Blink ReminderBlink;
    public float ReminderDuration;

    private void Start()
    {
        Dementia.FitHappened += OnFitHappened;
    }

    private void OnFitHappened(object sender, EventArgs e)
    {
        StartCoroutine(ShowCanvasCoroutine());
    }

    private IEnumerator ShowCanvasCoroutine()
    {
        ReminderBlink.ShouldBlink = true;
        ReminderBlink.Start();
        yield return new WaitForSeconds(ReminderDuration);
        ReminderBlink.ShouldBlink = false;
    }
}
