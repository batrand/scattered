﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoidSpawner : MonoBehaviour
{
    public DementiaMeter Dementia;
    public MindBlock[] StartingPoints;
    private int _currentIndex = 0;

    private void Start()
    {
        Dementia.FitHappened += OnFitHappened;
    }

    private void OnFitHappened(object sender, EventArgs e)
    {
        if (_currentIndex == StartingPoints.Length) return;
        StartingPoints[_currentIndex].StartDissolution();
        _currentIndex++;
    }
}

