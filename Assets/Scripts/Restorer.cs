﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Restorer : MonoBehaviour
{
    [Range(0,1)]
    public float HitStrength = 0.1f;
    
    public Collider2D RestorerCollider;

    private MindBlock _currentBlock;
    private Bounds _currentBlockBounds;
    private Bounds _restorerBounds;

    private void Start()
    {
        _restorerBounds = RestorerCollider.bounds;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("MindBlock"))
        {
            if(_currentBlock!=null) _currentBlock.Deselect();
            
            _currentBlock = other.gameObject.GetComponent<MindBlock>();

            if (_currentBlock.IsTotallyDissolved) return;
            if (!_currentBlock.HasStartedDissolving) return;

            Debug.Log("Found block");
            _currentBlock.Select();
            _currentBlockBounds = other.collider.bounds;
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && _currentBlock != null)
        {
            _currentBlock.RemoveDissolution(HitStrength);
        }

        if (_currentBlock != null)
        {
            _restorerBounds = RestorerCollider.bounds;
            if (!_restorerBounds.Intersects(_currentBlockBounds))
            {
                Debug.Log("Block gone!");
                _currentBlock.Deselect();
                _currentBlock = null;
            }
        }
    }
}
