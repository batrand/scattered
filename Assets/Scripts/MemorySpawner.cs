﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemorySpawner : MonoBehaviour
{
    public float SpawnInterval = 10;
    public GameObject MemoryPrefab;
    public Transform[] Locations;

    private void Start()
    {
        InvokeRepeating(nameof(Spawn), 0, SpawnInterval);
    }

    private void Spawn()
    {
        var index = Random.Range(0, Locations.Length - 1);
        var location = Locations[index];
        Instantiate(MemoryPrefab, location.position, Quaternion.identity);
    }
}
