﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public PlayerMovement Movement;
    public SpriteRenderer Renderer;
    public Sprite Idle;
    public Sprite Walk1;
    public Sprite Walk2;
    public float FlipWalkSeconds = 0.5f;

    private void Update()
    {
        if (!Movement.IsMoving)
        {
            StopCoroutine(FlipWalkCoroutine());
            Renderer.sprite = Idle;
        }
        else
        {
            if (!_isWalking) StartCoroutine(FlipWalkCoroutine());
        }
    }

    private bool _isWalking = false;
    private IEnumerator FlipWalkCoroutine()
    {
        _isWalking = true;
        Renderer.sprite = Walk1;
        yield return new WaitForSeconds(FlipWalkSeconds);
        Renderer.sprite = Walk2;
        yield return new WaitForSeconds(FlipWalkSeconds);
        _isWalking = false;
    }
}
