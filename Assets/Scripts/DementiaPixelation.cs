﻿using System.Collections;
using System.Collections.Generic;
using Assets.Pixelation.Scripts;
using UnityEngine;

public class DementiaPixelation : MonoBehaviour
{
    public DementiaMeter Dementia;
    public Pixelation Pixelator;

    private void Update()
    {
        // Will stop pixelation when dementia is more than half
        Pixelator.enabled = Dementia.RemainingSeconds < (Dementia.StartingSeconds/2);
    }
}
