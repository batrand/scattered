﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MindState : MonoBehaviour
{
    private MindBlock[] _mindBlocks;
    private int _remainingBlocks;
    public event EventHandler AllBlocksDissolved;
    private void Start()
    {
        _mindBlocks = FindObjectsOfType<MindBlock>();
        foreach (var block in _mindBlocks)
        {
            block.JustTotallyDissolved += OnBlockDissolved;
            _remainingBlocks = _mindBlocks.Length;
        }
    }

    private void OnBlockDissolved(object sender, EventArgs e)
    {
        _remainingBlocks--;
        Debug.Log("A block just dissolved!");
        if (_remainingBlocks == 0)
        {
            AllBlocksDissolved?.Invoke(this, null);
            Debug.Log("All blocks dissolved!");
        }
    }
}
