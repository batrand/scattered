﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackgroundMusic : MonoBehaviour
{
    private static BackgroundMusic _instance;
    public AudioSource Source;
    public AudioClip MenuClip;
    public AudioClip GameClip;
    private void Start()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        _instance = this;
        SceneManager.sceneLoaded += OnSceneLoaded;

        Source.clip = MenuClip;
        Source.loop = true;
        Source.Play();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        switch (scene.name)
        {
            case "MenuScene": PlayMenuMusic();
                break;
            case "GameScene": PlayGameMusic();
                break;
        }
    }

    private void PlayMenuMusic()
    {
        if (Source.clip == MenuClip) return;
        Source.Stop();
        Source.clip = MenuClip;
        Source.Play();
    }

    private void PlayGameMusic()
    {
        Source.Stop();
        Source.clip = GameClip;
        Source.Play();
    }
}
