﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolutionEffect : MonoBehaviour
{
    [Range(0,1)]
    public float Amount = 0;
    public SpriteRenderer Renderer;
    private Material _material;

    private void Start()
    {
        _material = Renderer.material;
    }

    private void Update()
    {
        _material.SetFloat("_Threshold", Amount);
    }

    public void SetAmount(float amount)
    {
        Amount = amount;
    }
}
